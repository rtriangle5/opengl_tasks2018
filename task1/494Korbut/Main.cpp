#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
 
#include <iostream>
#include <vector>
 
std::string data = "494KorbutData/";
 
MeshPtr makeTorus(float innerRadius, float outerRadius, unsigned int angleShapesNumber)
{
    float R = (innerRadius+outerRadius)/2;
    float r = (outerRadius-innerRadius)/2;
   
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    int N = angleShapesNumber;
    for (unsigned int i = 0; i < N; i++)
    {
        float theta = 2*(float)glm::pi<float>() * i / N;
        float theta1 = 2*(float)glm::pi<float>() * (i + 1) / N;
       
        for (unsigned int j = 0; j < N; j++)
        {
            float phi = 2*(float)glm::pi<float>() * j / N;
            float phi1 = 2*(float)glm::pi<float>() * (j + 1) / N;
            //Первый треугольник, образующий квад
            vertices.push_back(glm::vec3((R + r * cos(phi)) * cos(theta),
                                         (R + r * cos(phi)) * sin(theta),
                                         r * sin(phi)));
           
            vertices.push_back(glm::vec3((R + r * cos(phi)) * cos(theta1),
                                         (R + r * cos(phi)) * sin(theta1),
                                         r * sin(phi)));
           
            vertices.push_back(glm::vec3((R + r * cos(phi1)) * cos(theta),
                                         (R + r * cos(phi1)) * sin(theta),
                                         r * sin(phi1)));
           
            normals.push_back(glm::vec3(cos(phi)*cos(theta), cos(phi)*sin(theta), sin(phi)));
            normals.push_back(glm::vec3(cos(phi)*cos(theta1), cos(phi)*sin(theta1), sin(phi)));
            normals.push_back(glm::vec3(cos(phi1)*cos(theta), cos(phi1)*sin(theta), sin(phi1)));
           
            //Второй треугольник, образующий квад
            vertices.push_back(glm::vec3((R + r * cos(phi1)) * cos(theta1),
                                         (R + r * cos(phi1)) * sin(theta1),
                                         r * sin(phi1)));
           
            vertices.push_back(glm::vec3((R + r * cos(phi)) * cos(theta1),
                                         (R + r * cos(phi)) * sin(theta1),
                                         r * sin(phi)));
           
            vertices.push_back(glm::vec3((R + r * cos(phi1)) * cos(theta),
                                         (R + r * cos(phi1)) * sin(theta),
                                         r * sin(phi1)));
           
            normals.push_back(glm::vec3(cos(phi1)*cos(theta1), cos(phi1)*sin(theta1), sin(phi1)));
            normals.push_back(glm::vec3(cos(phi)*cos(theta1), cos(phi)*sin(theta1), sin(phi)));
            normals.push_back(glm::vec3(cos(phi1)*cos(theta), cos(phi1)*sin(theta), sin(phi1)));
        }
    }
   
    //----------------------------------------
   
    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());
   
    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());
   
   
    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());
   
    std::cout << "Torus is created with " << vertices.size() << " vertices\n";
   
    return mesh;
}
 
 
class SampleApplication : public Application
{
public:
    MeshPtr _torus;
    ShaderProgramPtr _shader;
   
    void makeScene() override {
        Application::makeScene();
       
        //_cameraMover = std::make_shared<FreeCameraMover>();
       
        _torus = makeTorus(innerRadius, outerRadius, (unsigned int)angleShapesNumber);
       
        _shader = std::make_shared<ShaderProgram>(data+"shader.vert", data+"shader.frag");
    }
   
    void update() override {
        if (glfwGetKey(_window, GLFW_KEY_MINUS) == GLFW_PRESS) {
            updateShapes(-1);
            _torus = makeTorus(innerRadius, outerRadius, (unsigned int)angleShapesNumber);
        }
       
        if (glfwGetKey(_window, GLFW_KEY_EQUAL) == GLFW_PRESS) {
            updateShapes(1);
            _torus = makeTorus(innerRadius, outerRadius, (unsigned int)angleShapesNumber);
        }
        Application::update();
    }
   
    void draw() override {
        Application::draw();
       
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);
       
        glViewport(0, 0, width, height);
       
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
       
        _shader->use();
       
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
       
        _shader->setMat4Uniform("modelMatrix", _torus->modelMatrix());
        _torus->draw();
    }
   
    SampleApplication(float innerRadius, float outerRadius, unsigned int angleShapesNumber, unsigned int pressDelta = 1): innerRadius(innerRadius), outerRadius(outerRadius), angleShapesNumber(angleShapesNumber), delta(pressDelta) {}
   
    SampleApplication(): innerRadius(1.0f), outerRadius(2.0f), angleShapesNumber(20), delta(0.1f) {}
   
private:
    float innerRadius;
    float outerRadius;
    float angleShapesNumber;
    float delta;
    const int MIN_SHAPES = 4;
    const int MAX_SHAPES = 1000;
   
   
    void updateShapes(int sign) {
       
        angleShapesNumber += delta * sign;
       
        if (angleShapesNumber < MIN_SHAPES) {
            angleShapesNumber = MIN_SHAPES;
        } else if (angleShapesNumber > MAX_SHAPES) {
            angleShapesNumber = MAX_SHAPES;
        }
    }
   
};
 
int main() {
    SampleApplication app;
    app.start();
   
    return 0;
}